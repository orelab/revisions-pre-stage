
// EXO3

function generateHTML(valeurs) {
    let counter = 0;
    let html = '';

    for (let i = 0; valeurs.length > i; i++) {

        html += '<tr>';

        for (let j = 0; j < valeurs[i].length; j++) {

            html += `
                <td data-row="${i}" data-col="${j}" 
                    class="${typeof valeurs[i][j]}"></td>`;

            if (typeof valeurs[i][j]=="string"
            || typeof valeurs[i][j]=="number") { 
                counter++;
            }
        }
        html += '</tr>';
    }
    document.getElementById('jeu').innerHTML = html;

    return counter;
}

// EXO4

function emptyHTML(){
    let numchar = document.getElementById('jeu').innerHTML;
    document.getElementById('jeu').innerHTML = "";

    return numchar.length;
}

// EXO5

function updateData(arr, x, y, value){
    let realY = arr.length - y - 1; // on commence plutôt par le bas de la colonne
    arr[realY][x] = value;
    return arr;
}

// EXO6

function columnValuesCount(arr, numCol){
    return arr.reduce((acc, val) => {
        if( typeof val[numCol]=="string"
        || typeof val[numCol]=="number"){
            return acc+1;
        }
        else return acc;
    }, 0);

}

