
// EXO1

let lignes = [
    [false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false],
    [false, false, false, 11111, false, false, false],
    [false, false, false, "rrr", false, false, false],
    [false, false, "rrr", 11111, 11111, "rrr", false]
];

let player = false;


// EXO3

let numcel = generateHTML(lignes);
console.log("nombre de cases : " + numcel);

// EXO4

let numdel = emptyHTML();
console.log("nombre de caractères supprimés : " + numdel);

// EXO5

lignes = updateData(lignes, 1, 0, "rrr"); // en fait, pas besoin de capturer lignes au retour de fonction car lignes est passé à la fonction par référence
generateHTML(lignes);

// EXO6

console.log("nombre de valeurs à la ligne 3 : " + columnValuesCount(lignes, 3));
console.log("nombre de valeurs à la ligne 5 : " + columnValuesCount(lignes, 5));

// EXO7

document.querySelector('#update')
    .addEventListener('click', () => {
        console.log('interface updated !');
        generateHTML(lignes);
    })

// EXO8

document.querySelector('#jeu')
    .addEventListener('click', (e) => {
        let col = e.target.dataset.col;
        let row = columnValuesCount(lignes, col);

        console.log(col);

        updateData(lignes, col, row, player?"rrr":1111);

        player = !player;

        generateHTML(lignes);
    })